(ns ift.test
  (:refer-clojure :exclude [compile]))

(declare compile)

(defn smoosh
  [form]
  (filterv
    #(and (vector? %) (not (vector? (first %))))
    (tree-seq sequential? seq form)))

(defn expand
  [form]
  (if-not (vector? form)
    form
    (let [[tx-element & rest] form]
      (cond
        (= #{:<> :->} tx-element)
        (into [tx-element] (map expand rest))
        (fn? tx-element)
        (expand (apply tx-element rest))
        :else (mapv expand form)))))

(defn spread*
  [form]
  (println form)
  (if (vector? form)
    (reduce
      (fn [acc form]
        (if (vector? form)
          (if (= (first form) :<>)
            (into acc (rest (spread* form)))
            (conj acc (spread* form)))
          (conj acc form)))
      []
      form)
    form))

(defn spread
  [form]
  (let [form (spread* form)]
    (if (and (vector? form) (= :<> (first form)))
      (into [] (rest form))
      form)))


(defn postprocess
  [form tempid-fn & [tempid]]
  (if (-> form first vector?)
    (mapv #(postprocess %1 tempid-fn) form)
    (let [tempid (or tempid (tempid-fn))
          [tx-kw & rest] form]
      (cond
        (= :<> tx-kw)
        (into [:<>] (mapv #(postprocess % tempid-fn tempid) rest))

        (= :-> tx-kw)
        (reduce
          (fn [acc form])

          (mapv #(postprocess % tempid-fn nil) rest))

        :else
        (into [tx-kw tempid] rest)))))

(defn compile
  ([form]
   (compile form gensym))
  ([form tempid-fn]
   (-> form expand (postprocess tempid-fn) spread smoosh)))

(defn add-name
  [full-name]
  (let [[fname lname] (clojure.string/split full-name #"\s+")]
    [:<>
     [:db/add :asdf/first-name fname]
     [:db/add :asdf/last-name lname]]))

(defn add-road
  [road]
  [:db/add :asdf/road road])

(defn add-address
  [address]
  [:<>
   [:db/add :asdf/address address]
   [add-road "some road"]])

(defn add-parent
  [parent-id]
  [:db/add :asdf/parent parent-id])

(defn add-person
  [{:keys [name address]} parent-id]
  [:<>
   [add-name name]
   [add-address address]
   [add-parent parent-id]])

(defn add-parent-child
  [parent child]
  [:->
   [add-person parent nil]
   [add-person child nil]])

#_(compile [add-person {:name "Luke Cook" :address "some address"}])
