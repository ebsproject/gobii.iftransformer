# ift

## Build

Install Leiningen from https://leiningen.org/

```bash
lein uberjar
```

## Usage
```bash
lein run usage-type instruction-file-type from-version to-version instruction-file-path

usage-type: -ff read file, write file
            -fs read file, write to system out
            -sf read system in, write to file
            -ss read system in, write to system out

instruction-file-type: extract | digest
from-version: v1 | v2
to-version: v1 | v2
```
## Supported Versions

### Decoding
```
Extract: V1, V2
Digest: V1, V2
```
### Encoding
```
Extract: V2
Digest: V2
```
